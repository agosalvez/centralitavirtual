# README

Proyecto TFG2018

Middleware centralita virtual PBX sobre Asterisk

* Ruby on Rails

* Creación de departamentos, órdenes de timbrado

* Control de numeración externa, interna

* Controles antifraute por hackVoIP

* Control de fichado de empleados

* Servicio web para ofrecer estadisticas y estados de llamadas a tiempo real

* Personalización de horario, locuciones, buzones de voz, etc...

* ...
