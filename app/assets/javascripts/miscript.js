jQuery(function($) {
    $(document).ready(function() {
        //alert('jquery ready!');

        // Vars
        var $newEmployee = '.new-employee',
            $inputEmpleado = '#empleadoInput',
            $deleteExtension = '.deleteExtension',
            $btnEditEmployee = '.btnEditEmployee',
            $btnEditTimeDpto = '.btnEditTimeDpto',
            $btnEditModeDpto = '.btnEditModeDpto',
            $submitEditExtension = '.submitEditExtension',
            $submitEditTimeDpto = '.submitEditTimeDpto',
            $submitEditModeDpto = '.submitEditModeDpto',
            $btnAddExtensions = '.btnAddExtensions',
            $btnSubmitAddExten = '.btnSubmitAddExten',
            $deleteDepartamento = '.deleteDepartamento';


        // Triggers
        $($newEmployee).click(function(){ nuevoEmpleado($(this)) });
        $($deleteExtension).click(function(){ hacerSubmit($(this), 1) });
        $($btnEditEmployee).click(function(){ editEmployee($(this)) });

            // -- Editar empleado de una extensión
        $($btnEditEmployee).click(function(){ editEmployee($(this)) });
        $($submitEditExtension).click(function(){ hacerSubmit($(this), 0) });

            // -- Editar tiempo de timbrado de un departamento
        $($btnEditTimeDpto).click(function(){ hacerSubmit($(this), 0) });

        // -- Editar modo de timbrado de un departamento
        $($btnEditModeDpto).click(function(){ hacerSubmit($(this), 0) });

        $($btnAddExtensions).click(function(){ addExtensionsToDpto($(this)) });
        $($submitEditExtension).click(function(){ hacerSubmit($(this), 0) });

        $($btnSubmitAddExten).click(function(){ hacerSubmit($(this), 0) });

        $($deleteDepartamento).click(function(){ hacerSubmit($(this), 1)})

        // Functions
        /*
        * Función que muestra el mini-form de nuevo empleado
        */
        function nuevoEmpleado($j) {
            $($inputEmpleado).show();
            $j.hide();
        }
        /*
        * Función que hace submit de un formulario
        * $j: El propio objeto pinchado o clicado. Hace submit del form mas cercano.
        */
        function hacerSubmit($j, confirm) {
            event.preventDefault()
            if (confirm) {
                if (window.confirm("¿Estás seguro?")) {
                    $j.closest('form').submit();
                }
            } else {
                $j.closest('form').submit();
            }
        }
        /*
        * Mini-form interactivo para editar un empleado de una extensión.
        */
        function editEmployee($j) {
            var id = $j.data("id");
            $("#nameEmployee"+id).hide();
            $(".inputEditEmployee"+id).show();
            $j.hide();
        }
        /*
        * Mini-form interactivo para añadir extensiones a un departamento.
        */
        function addExtensionsToDpto($j) {
            var id = $j.data("id");
            $("#extensionsDpto"+id).hide();
            $('span.containerSelect'+id).show();
            $('.btnSubmitAddExtensionDpto'+id).show();
            $j.hide();
        }
    });
});