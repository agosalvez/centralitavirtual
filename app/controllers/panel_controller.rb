class PanelController < ApplicationController
  include ExtensionesHelper

    def test

      crearDepartamento('tecnico')
      crearDepartamento('comercial')
      crearExtension('101')
      crearExtension('102')
      crearExtension('103')

      redirect_to "/extensiones"
    end

    def extensiones
      @Telefonos = Telefono.all
      @Departamentos = getExtensionsByDptos
    end

    def createExtensions
      empleadoSelect = params['empleado']
      nuevoEmpleado = params['empleadoInput']
      empleado = (nuevoEmpleado != '') ? nuevoEmpleado : empleadoSelect
      if crearExtension(params['numero'], empleado) then
        notice = 'Extensión creada correctamente'
      else
        notice = 'Error al crear la extensión'
      end
      redirect_to "/extensiones", notice: notice
    end

    # DELETE /extensiones
    def deleteExtensions
      borrarExtension(params['idExtension'])
      redirect_to "/extensiones"
    end

    #PUT /empleados
    def editEmployees
      id = params['idExtension']
      empleado = params['nuevoEmpleado']
      editEmployee(id, empleado)
      redirect_to "/extensiones"
    end

    #GET /departamentos
    def departamentos
      @Telefonos = Telefono.all
      @Departamentos = Departamento.all
      @Extensiones = getExtensionsByDptos
    end

    #POST /departamentos
    def createDepartments
      nombre = params['dptoname']
      tiempo = params['dptotime']
      if crearDepartamento(nombre) then
        notice = 'Departamento creada correctamente'
      else
        notice = 'Error al crear el departamento'
      end
      redirect_to '/departamentos', notice: notice
    end

    def editTimeDepartament
      params['dialplan'].each do |dpto, time|
        editTimeDpto(dpto, time)
      end
      redirect_to '/departamentos'
    end

    def editModeDepartament
      params['dialplan'].each do |dpto, mode|
        editModeDpto(dpto, mode)
      end
      redirect_to '/departamentos'
    end

    def addExtensionsToDpto
      id = params['idDpto']
      extensions = params['extensiones']
      if addExtensToDpto(id, extensions) then
        notice = 'Extensiones asignadas correctamente al departamento'
      else
        notice = 'Error al asignar las extensiones'
      end
      redirect_to '/departamentos', notice: notice
    end

    # DELETE /departamentos
    def deleteDepartamentos
      borrarDepartamento(params['idDpto'])
      redirect_to "/departamentos"
    end
end
