module CentralitaHelper

  def random(cantidad, tipo)
    if cantidad > 0 then
      if tipo then
        return (0...cantidad).map { ('a'..'z').to_a[rand(26)] }.join
      else
        return (0...cantidad).map { (0..9).to_a[rand(10)] }.join
      end
    else
      return -1
    end
  end

  def getExtensions
    return Extension.all
  end

  def getSipusers
    return Sipuser.all
  end

  # Inicializa la tabla extensions con la base inicial de la centralita
  def createCentralita
    exten = ['t', 't', 'i', 'i', 'h', 'h', 'h', '_XXX', '000']
    priority = ['1', '2', '1', '2', '1', '2', '3', '1', '1']
    app = ['Playback', 'Hangup', 'Playback', 'Hangup', 'Set', 'Set', 'Hangup', 'Goto', 'Playback']
    appdata = ['demo-nomatch', '', 'invalid', '', 'CDR(accountcode)=${CONTEXT}', 'CDR(userfield)=${I_USER}', '', Telefono::CONTEXT + ',' + Telefono::CONTEXT.downcase + '${EXTEN},1', 'tt-monkeys']
    inicio = 1
    fin = appdata.count
    (inicio..fin).each do |i|
      existe = Extension.find_by_sql("SELECT COUNT(*) as cantidad FROM extensions WHERE context = '" + Telefono::CONTEXT + "' AND exten = '" + exten[i-1] + "' AND priority = " + priority[i-1])
      if existe[0].cantidad == 0 then
        obj = Extension.new({'context': Telefono::CONTEXT,
                             'exten': exten[i-1],
                             'priority': priority[i-1],
                             'app': app[i-1],
                             'appdata': appdata[i-1]
                            })
        obj.save
      end
    end
  end

  # Borra toda la centralita(Telefono::CONTEXT)
  def deleteCentralita
    Extension.where(context: Telefono::CONTEXT).destroy_all
    Sipuser.where(context: Telefono::CONTEXT).destroy_all
  end

  # Crea una extensión(exten, password, employee_id)
  def createExtension(exten, password, employee_id=-1)
    resultExt = false
    resultSip = false
    allow = 'alaw;ulaw;gsm;ilbc'

    # Tabla Extensions
    # Se crea una extensión en extensions
    app = ['Set','Dial','Playback','Hangup']
    appdata = ['I_USER=${EXTEN}','SIP/${EXTEN},120,ort','followme/sorry','']
    inicio = 1
    fin = appdata.count
    (inicio..fin).each do |i|
      existe = Extension.find_by_sql("SELECT COUNT(*) as cantidad FROM extensions WHERE context = '"+Telefono::CONTEXT+"' AND exten = '"+Telefono::CONTEXT.downcase + exten+"' AND priority = #{i}")

      if existe[0].cantidad == 0 then
        obj = Extension.new({'context': Telefono::CONTEXT,
                             'exten': Telefono::CONTEXT.downcase + exten,
                             'priority': i,
                             'app': app[i-1],
                             'appdata': appdata[i-1]
                            })
        resultExt = obj.save
      end
    end

    # Tabla Sipusers
    # Se crea la extensión en sipusers
    peer = Sipuser.find_by_sql("SELECT COUNT(*) as cantidad FROM sipusers WHERE context = '"+Telefono::CONTEXT+"' AND name = '"+Telefono::CONTEXT.downcase + exten+"'")
    if peer[0].cantidad == 0 then
      obj = Sipuser.new({'name': Telefono::CONTEXT.downcase + exten,
                         'context': Telefono::CONTEXT,
                         'callingpres': 'allowed_not_screened',
                         'secret': password,
                         'call-limit': 10,
                         'host': 'dynamic',
                         'nat': 'no',
                         'typepeer': 'friend',
                         'accountcode': Telefono::CONTEXT,
                         'dtmfmode': 'rfc2833',
                         'insecure': 'no',
                         'language': 'es',
                         'mailbox': Telefono::CONTEXT.downcase + exten,
                         'disallow': 'all',
                         'allow': allow,
                         'defaultuser': Telefono::CONTEXT.downcase + exten,
                         'allowoverlap': 'yes',
                         'allowsubscribe': 'yes',
                         'allowtransfer': 'yes',
                         'ignoresdpversion': 'no',
                         'videosupport': 'no',
                         'rfc2833compensate': 'yes',
                         'session-timers':'yes',
                         'session-expires':1800,
                         'session-minse':90,
                         'session-refresher':'uas',
                         'registertrying':'yes',
                         'qualifyfreq':120,
                         'lastms':'0',
                         #'employee_id':employee_id,
                        })
      resultSip = obj.save
    end

    if resultExt && resultSip then
      return true
    else
      return false
    end
  end

  # Borra una extensión por el número de extensión(exten)
  def deleteExtension(exten)
    Extension.where(exten: Telefono::CONTEXT.downcase + exten).destroy_all
    Sipuser.where(name: Telefono::CONTEXT.downcase + exten).destroy_all
  end

  def getIdByNumero(numero)
    return Sipuser.find_by_name(Telefono::CONTEXT.downcase + numero).id
  end

  def createDialplan(departamentos, extensiones)
    # para idDpto 1 -> s1, idDpto 2 -> s2, etc... (s- + idDpto + -nombreDpto)
    # s-1-tecnico

    Extension.connection.execute("DELETE FROM extensions WHERE exten LIKE 's%'")

    departamentos.each do |d|
      exten = 's' + d.id.to_s
      vueltas = 1
      tiempo = d.tiempo_timbrado.to_s
      modo = d.modo_timbrado
      dialling = ''

      if d.modo_timbrado == 's' #simultaneo
        # e[0] -> departamento
        # e[1] -> array de extensiones asociadas
        extensiones.each do |e|
          if e[0] == d.id.to_s
            e[1].each do |idTelefono|
              dialling = dialling + 'SIP/' + Telefono::CONTEXT.downcase + Telefono.find(idTelefono).numero + '&'
            end
            dialling = dialling + ',' + tiempo + ',ort'
          end
        end
        dialling.sub! '&,', ','
        exten = 's' + d.id.to_s
        app = ['Set', 'While', 'Set', 'Dial', 'Set', 'EndWhile', 'Set', 'Goto', 'Hangup']
        appdata = ['i=' + vueltas.to_s, '$[${i} > 0]', 'I_USER=' + Telefono::CONTEXT.downcase, dialling, 'i=$[${i} - 1]', '', 'I_USER='+ Telefono::CONTEXT.downcase + '_unattended', 'h,1', '']

        inicio = 1
        fin = appdata.count
        (inicio..fin).each do |i|
          obj = Extension.new({'context': Telefono::CONTEXT,
                               'exten': exten,
                               'priority': i,
                               'app': app[i-1],
                               'appdata': appdata[i-1]
                              })
          obj.save
        end
      elsif d.modo_timbrado == 'c' #consecutivo
        exten = 's' + d.id.to_s
        priority = 1

        app = ['Set', 'While']
        appdata = ['i='+ vueltas.to_s, '$[${i} > 0]']

        # Bucle inicial
        inicio = 1
        fin = appdata.count
        (inicio..fin).each do |j|
          obj = Extension.new({'context': Telefono::CONTEXT,
                               'exten': exten,
                               'priority': priority,
                               'app': app[j-1],
                               'appdata': appdata[j-1]
                              })
          priority=priority+1
          obj.save
        end

        # Bucle repetición
        extensiones.each do |e|
          if e[0] == d.id.to_s
            e[1].each do |ee|
              obj = Extension.new({'context': Telefono::CONTEXT,
                                   'exten': exten,
                                   'priority': priority,
                                   'app': 'Set',
                                   'appdata': 'I_USER=' + Telefono::CONTEXT.downcase + Telefono.find(ee).numero
                                  })
              obj.save
              priority=priority+1
              obj = Extension.new({'context': Telefono::CONTEXT,
                                   'exten': exten,
                                   'priority': priority,
                                   'app': 'Dial',
                                   'appdata': 'SIP/' + Telefono::CONTEXT.downcase + Telefono.find(ee).numero + ',' + tiempo + ',ort'
                                  })
              priority=priority+1
              obj.save
            end
          end
        end

        app2 = ['Set', 'EndWhile', 'Set', 'Goto', 'Hangup']
        appdata2 = ['i=$[${i} - 1]', '', 'I_USER='+ Telefono::CONTEXT.downcase + '_unattended', 'h,1', '']

        # Bucle final
        inicio = 1
        fin = appdata2.count
        (inicio..fin).each do |i|
          obj = Extension.new({'context': Telefono::CONTEXT,
                               'exten': exten,
                               'priority': priority,
                               'app': app2[i-1],
                               'appdata': appdata2[i-1]
                              })
          priority=priority+1
          obj.save
        end
      end
    end
  end

  # Función que migra toda la configuración de las entidades propias a lenguaje de Asterisk
  def syncAsterisk(type, exten = nil)
    # Extensiones
    case type
      when 'crear'
        self.inicializarCentralita
        # Crear extensiones
        # Pasar Telefonos a sipusers mediante centralita_helper.rb
        peers = Telefono.all
        peers.each do |p|
          createExtension(p.numero, p.password)
        end
      when 'borrar'
        deleteExtension(exten)
      else
    end
  end

end
