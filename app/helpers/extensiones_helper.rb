module ExtensionesHelper

  include CentralitaHelper

  def inicializarCentralita
    createCentralita
    crearDepartamentoDefault
  end

  def borrarExtension(numero)
    Telefono.where(numero: numero).destroy_all
  end

  def getTelefonos
    return Telefono.all
  end

  def random(cantidad, tipo)
    if cantidad > 0 then
      if tipo then
        return (0...cantidad).map { ('a'..'z').to_a[rand(26)] }.join
      else
        return (0...cantidad).map { (0..9).to_a[rand(10)] }.join
      end
    else
      return -1
    end
  end

  def crearExtension(numero, empleado = '')
    # Crear extension en own
    crearDepartamentoDefault
    regex = /^\d{3}$/
    if regex.match(numero) != nil then
      peer = Telefono.find_by_sql("SELECT COUNT(*) as cantidad FROM telefonos WHERE numero = '"+numero+"'")
      if peer[0].cantidad == 0 then
        obj = Telefono.new({'numero': numero,
                              'password': random(5, false) + "." + random(10, true),
                              'activa':true,
                              'empleado':empleado,
                              'departamento_id':Departamento.first
                             })
        resultOwn = obj.save
      end
      syncAsterisk('crear')
      crearOrdenDeTimbradoDeDptos
      if resultOwn then
        return true
      end
    else
      return false
    end
  end

  def borrarExtension(id)
    exten = getExtenByIdOwn(id)
    obj = Dptoexten.find_by_sql("SELECT * FROM dptoextens WHERE telefono_id = #{id}")
    Dptoexten.delete(obj)
    Telefono.destroy(id)
    syncAsterisk('borrar', exten)
    crearOrdenDeTimbradoDeDptos
  end

  def editEmployee(id, employee)
    Telefono.update(id, :empleado => employee)
  end

  def getIdByExtenOwn(exten)
    return Telefono.find_by_numero(exten).id
  end

  def getExtenByIdOwn(id)
    return Telefono.find(id).numero
  end

  def crearDepartamentoDefault
    dpto = Departamento.find_by_sql("SELECT COUNT(*) as cantidad FROM departamentos WHERE nombre = '" + Departamento::DEPARTAMENTO + "'")
    if dpto[0].cantidad == 0 then
      obj = Departamento.new({
          'nombre':Departamento::DEPARTAMENTO,
          'modo_timbrado':'s',
          'tiempo_timbrado':120,
          'locucion':false
                          })
      obj.save
    end
  end

  def crearDepartamento(nombre)
    crearDepartamentoDefault
    dpto = Departamento.find_by_sql("SELECT COUNT(*) as cantidad FROM departamentos WHERE nombre = 'nombre'")
    if dpto[0].cantidad == 0 then
      obj = Departamento.new({'nombre': nombre,
                              'modo_timbrado': 's',
                              'tiempo_timbrado':120,
                              'locucion':false
                             })
      return obj.save
    end
    syncAsterisk('crear')
    crearOrdenDeTimbradoDeDptos
    if resultOwn then
      return true
    end
  end

  def editTimeDpto(id, time)
    Departamento.update(id, :tiempo_timbrado => time)
    crearOrdenDeTimbradoDeDptos
  end

  def editModeDpto(id, mode)
    Departamento.update(id, :modo_timbrado => mode)
    crearOrdenDeTimbradoDeDptos
  end

  def addExtensToDpto(idDpto, extensions)
    # Borro las relaciones del departamento
    Dptoexten.where(:departamento_id => idDpto).destroy_all
    if extensions != nil then
      extensions.each do |e|
        rel = Dptoexten.new({
                                'departamento_id':idDpto,
                                'telefono_id':e
                            })
        rel.save
      end
    end
    crearOrdenDeTimbradoDeDptos
  end

  def getExtensionsByDptos
    dptos = Array.new
    Departamento.all.each do |d|
      Telefono.all.each do |t|
        cant = Dptoexten.find_by_sql("SELECT COUNT(*) as cantidad FROM dptoextens WHERE departamento_id = #{d.id} AND telefono_id = #{t.id}")
        if cant[0].cantidad == 1
          lista = [d.id, t.id]
          dptos.push(lista)
        end
      end
    end
    return dptos
  end

  def getExtensionsByDpto(idDpto)
  result = Array.new
  rows = Dptoexten.find_by_sql("SELECT telefono_id FROM dptoextens WHERE departamento_id = " + idDpto.to_s)
  rows.each do |r|
    result.push(r.telefono_id)
  end
  return result
end

  def borrarDepartamento(id)
    Dptoexten.where(:departamento_id => id).destroy_all
    Departamento.where(:id => id).destroy_all
    crearOrdenDeTimbradoDeDptos
  end

  def crearOrdenDeTimbradoDeDptos
    departamentos = Departamento.all
    extensiones = Array.new
    departamentos.each do |d|
      tupla = getExtensionsByDpto(d.id)
      extensiones.push([d.id.to_s, tupla])
    end
    createDialplan(departamentos, extensiones)
  end

end

