json.extract! extension, :id, :context, :exten, :priority, :app, :appdata, :created_at, :updated_at
json.url extension_url(extension, format: :json)
