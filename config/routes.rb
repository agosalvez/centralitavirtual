Rails.application.routes.draw do

  # PANEL
  get '/extensiones', to: 'panel#extensiones'
  get '/', to: 'panel#extensiones'

  post '/extensiones', to: 'panel#createExtensions'
  delete '/extensiones', to: 'panel#deleteExtensions'

  put '/empleados', to: 'panel#editEmployees'

  get '/departamentos', to: 'panel#departamentos'
  post '/departamentos', to: 'panel#createDepartments'
  put '/departamentosTime', to: 'panel#editTimeDepartament'
  put '/departamentosMode', to: 'panel#editModeDepartament'
  put '/departamentosExtensiones', to: 'panel#addExtensionsToDpto'
  delete '/departamentos', to: 'panel#deleteDepartamentos'

  # TEST
  get '/test', to: 'panel#test'

  # API
  namespace :api do
      resources :pbxapi, only: [:index, :create, :destroy, :update, :show]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
