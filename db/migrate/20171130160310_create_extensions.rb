class CreateExtensions < ActiveRecord::Migration[5.1]
  def change
    create_table :extensions do |t|
      t.string :context
      t.string :exten
      t.string :priority
      t.string :app
      t.string :appdata

      t.timestamps
    end
  end
end
