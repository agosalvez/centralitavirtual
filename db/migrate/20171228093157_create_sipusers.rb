class CreateSipusers < ActiveRecord::Migration[5.1]
  def change
    create_table :sipusers do |t|
      t.string :name
      t.string :context
      t.string :callingpres
      t.string :deny
      t.string :permit
      t.string :secret
      t.integer :'call-limit'
      t.string :md5secret
      t.string :remotesecret
      t.string :transport
      t.string :host
      t.string :nat
      t.string :typepeer
      t.string :accountcode
      t.string :amaflags
      t.string :callgroup
      t.string :callerid
      t.string :defaultip
      t.string :dtmfmode
      t.string :fromuser
      t.string :fromdomain
      t.string :insecure
      t.string :language
      t.string :mailbox
      t.string :pickupgroup
      t.string :qualify
      t.string :regexten
      t.string :rtptimeout
      t.string :rtpholdtimeout
      t.string :setvar
      t.string :disallow
      t.string :allow
      t.string :fullcontact
      t.string :ipaddr
      t.integer :port
      t.string :defaultuser
      t.string :subscribecontext
      t.string :useragent
      t.string :directmedia
      t.string :trustrpid
      t.string :sendrpid
      t.string :progressinband
      t.string :promiscredir
      t.string :useclientcode
      t.string :callcounter
      t.integer :busylevel
      t.string :allowoverlap
      t.string :allowsubscribe
      t.string :allowtransfer
      t.string :ignoresdpversion
      t.string :template
      t.string :videosupport
      t.integer :maxcallbitrate
      t.string :rfc2833compensate
      t.string :'session-timers'
      t.integer :'session-expires'
      t.integer :'session-minse'
      t.string :'session-refresher'
      t.string :'t38pt_udptl'
      t.string :outboundproxy
      t.string :callbackextension
      t.string :registertrying
      t.integer :timert1
      t.integer :timerb
      t.integer :qualifyfreq
      t.string :contactpermit
      t.string :contactdeny
      t.string :regserver
      t.integer :regseconds
      t.integer :lastms
      t.string :otroModelo
      t.string :activadaComprobacion
      t.string :tipoDispositivo

      t.timestamps
    end
  end
end
