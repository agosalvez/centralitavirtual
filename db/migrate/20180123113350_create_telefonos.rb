class CreateTelefonos < ActiveRecord::Migration[5.1]
  def change
    create_table :telefonos do |t|
      t.string :numero
      t.string :password
      t.boolean :activa
      t.string :empleado
      t.references :departamento

      t.timestamps
    end
  end
end
