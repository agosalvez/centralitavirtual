class CreateDepartamentos < ActiveRecord::Migration[5.1]
  def change
    create_table :departamentos do |t|
      t.string :nombre
      t.string :modo_timbrado
      t.integer :tiempo_timbrado
      t.boolean :locucion

      t.timestamps
    end
  end
end
