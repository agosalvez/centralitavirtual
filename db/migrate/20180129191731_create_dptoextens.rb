class CreateDptoextens < ActiveRecord::Migration[5.1]
  def change
    create_table :dptoextens do |t|
      t.integer :departamento_id
      t.integer :telefono_id

      t.timestamps
    end
  end
end
