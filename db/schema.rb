# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180129191731) do

  create_table "departamentos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "nombre"
    t.string "modo_timbrado"
    t.integer "tiempo_timbrado"
    t.boolean "locucion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dptoextens", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.integer "departamento_id"
    t.integer "telefono_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "extensions", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "context"
    t.string "exten"
    t.string "priority"
    t.string "app"
    t.string "appdata"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sipusers", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "name"
    t.string "context"
    t.string "callingpres"
    t.string "deny"
    t.string "permit"
    t.string "secret"
    t.integer "call-limit"
    t.string "md5secret"
    t.string "remotesecret"
    t.string "transport"
    t.string "host"
    t.string "nat"
    t.string "typepeer"
    t.string "accountcode"
    t.string "amaflags"
    t.string "callgroup"
    t.string "callerid"
    t.string "defaultip"
    t.string "dtmfmode"
    t.string "fromuser"
    t.string "fromdomain"
    t.string "insecure"
    t.string "language"
    t.string "mailbox"
    t.string "pickupgroup"
    t.string "qualify"
    t.string "regexten"
    t.string "rtptimeout"
    t.string "rtpholdtimeout"
    t.string "setvar"
    t.string "disallow"
    t.string "allow"
    t.string "fullcontact"
    t.string "ipaddr"
    t.integer "port"
    t.string "defaultuser"
    t.string "subscribecontext"
    t.string "useragent"
    t.string "directmedia"
    t.string "trustrpid"
    t.string "sendrpid"
    t.string "progressinband"
    t.string "promiscredir"
    t.string "useclientcode"
    t.string "callcounter"
    t.integer "busylevel"
    t.string "allowoverlap"
    t.string "allowsubscribe"
    t.string "allowtransfer"
    t.string "ignoresdpversion"
    t.string "template"
    t.string "videosupport"
    t.integer "maxcallbitrate"
    t.string "rfc2833compensate"
    t.string "session-timers"
    t.integer "session-expires"
    t.integer "session-minse"
    t.string "session-refresher"
    t.string "t38pt_udptl"
    t.string "outboundproxy"
    t.string "callbackextension"
    t.string "registertrying"
    t.integer "timert1"
    t.integer "timerb"
    t.integer "qualifyfreq"
    t.string "contactpermit"
    t.string "contactdeny"
    t.string "regserver"
    t.integer "regseconds"
    t.integer "lastms"
    t.string "otroModelo"
    t.string "activadaComprobacion"
    t.string "tipoDispositivo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "telefonos", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "numero"
    t.string "password"
    t.boolean "activa"
    t.string "empleado"
    t.bigint "departamento_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["departamento_id"], name: "index_telefonos_on_departamento_id"
  end

end
