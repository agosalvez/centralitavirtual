# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#

  #Extension.destroy_all

  # Extension.create(context: 'Mr. Peabody', exten: 'hola', priority: 1, app: 'adios', appdata: 'holamundo')

  #20.times do |index|
  #   Extension.create!(context: Faker::BackToTheFuture.character,
  #                 exten: Faker::Name.name,
  #                 priority: Faker::Number.digit,
  #                 app: Faker::Name.name,
  #                 appdata: Faker::Lorem.sentence)
  #end

  # p "Creadas #{Extension.count} tuplas"


# Cargo datos al seed desde el .yml

  # Tabla extensions

    #seed_file = Rails.root.join('db', 'seeds', 'extensions.yml')
    #config = YAML::load_file(seed_file)
    #Extension.create!(config)

    #c = CentralitaHelper.new
    #c.createCentralita

  # Tabla sipusers

    #c.createExtension('000')
    ##c.createExtension('901')
    ##c.createExtension('902')

    #seed_file = Rails.root.join('db', 'seeds', 'sipusers.yml')
    #config = YAML::load_file(seed_file)
    #Sipuser.create!(config)


    include ExtensionesHelper

    # Inicio centralita

    syncAsterisk('crear')

    # Extensiones

    crearExtension('101', 'Adrián')
    crearExtension('102', 'Alberto')
    crearExtension('103', 'Pablo')

    # Departamentos

    crearDepartamento('Técnico')
    crearDepartamento('Administración')

    # Departamento - Extensión
    #addExtensToDpto(1, [1,2,3])
    addExtensToDpto(2, [1,2])
    addExtensToDpto(3, [3])
