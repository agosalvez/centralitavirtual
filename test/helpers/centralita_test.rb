require 'test_helper'
include CentralitaHelper

class CentralitaTest < ActiveSupport::TestCase

  test "get extensions" do
    extensions = getExtensions.blank?
    assert_equal true, extensions
  end

  test "get sipsuers" do
    sipusers = getSipusers.blank?
    assert_equal true, sipusers
  end

  test "create centralita initial" do
    createCentralita

    assert_equal true, Extension.exists?(context: 'PBXTFG')
  end

  test "create centralita initial bis" do
    createCentralita
    ext = Extension.where("exten = '_XXX'")
    result = ext[0].appdata
    response = 'PBXTFG,pbxtfg${EXTEN},1'

    assert_equal response, result
  end

  test "destroy centralita" do
    createCentralita
    createExtension('201', 'pass')
    createExtension('202', 'pass')
    createExtension('203', 'pass')
    createExtension('204', 'pass')
    deleteCentralita

    assert_equal false, Extension.exists?(exten: 'pbxtfg204')
  end

  test "create extension 202" do
    createExtension('202', 'pass')
    ext = Extension.where("exten like 'pbxtfg202' AND app = 'Dial'")
    result = ext[0].appdata
    response = 'SIP/${EXTEN},120,ort'

    assert_equal response, result
  end

  test "destroy extension 202" do
    createExtension('202', 'pass')
    deleteExtension('202')

    assert_equal false, Extension.exists?(exten: 'pbxtfg202')
  end

  test "destroy extension 202 sipusers" do
    createExtension('202', 'pass')
    deleteExtension('202')

    assert_equal false, Sipuser.exists?(name: 'pbxtfg202')
  end
end
