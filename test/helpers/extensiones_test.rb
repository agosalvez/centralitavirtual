require 'test_helper'
include ExtensionesHelper

class ExtensionesTest < ActiveSupport::TestCase

  test "get ownsipsuers" do
    sipusers = getTelefonos.blank?
    assert_equal true, sipusers
  end

  test "inizializar centralita bis" do
    inicializarCentralita
    ext = Extension.where("exten = '_XXX'")
    result = ext[0].appdata
    response = Telefono::CONTEXT + ',' + Telefono::CONTEXT.downcase + '${EXTEN},1'

    assert_equal response, result
  end

  test "destroy centralita" do
    crearExtension('204')
    Telefono.destroy_all
    assert_equal false, Telefono.exists?(numero: '204')
  end

  test "create extension 202" do
    crearExtension('202')
    ext = Telefono.where("numero like 202")
    result = ext[0].numero
    response = '202'

    assert_equal response, result
  end

  test "destroy extension 202" do
    crearExtension('202')
    exten = Telefono.find_by_numero('202')
    id = getIdByExtenOwn('202')
    borrarExtension(id)

    assert_equal false, Telefono.exists?(numero: '202')
  end

  test "create extension with employee" do
    crearExtension('101', 'adrian')
    ext = Telefono.where("numero like 101 and empleado = 'adrian'")
    result = ext[0].empleado
    response = 'adrian'

    assert_equal response, result
  end

  test "edit employee frome extension" do
    crearExtension('101', 'adrian')
    ext = Telefono.where("numero like 101")
    editEmployee(ext[0].id, 'nuevoNombre')
    ext2 = Telefono.where("numero like 101")
    result = ext2[0].empleado
    response = 'nuevoNombre'

    assert_equal response, result
  end

  test "create departamento default" do
    crearDepartamentoDefault
    dpto = Departamento.where("nombre = '#{Departamento::DEPARTAMENTO}'")
    result = dpto[0].nombre
    response = Departamento::DEPARTAMENTO

    assert_equal response, result
  end

  #test "add extensiones departamento" do
  #  nombre = 'tecnico'
  #  crearDepartamento(nombre)
  #  idDpto = Departamento.where(:nombre => nombre)[0].id
  #  crearExtension('101')

  #  addExtensToDpto(idDpto, [1])

  #  response = Dptoexten.where(:departamento_id => idDpto)

  # assert true
  #end

  test "delete departamento" do
    nombre = 'comercial'
    ExtensionesHelper.crearDepartamento(nombre)
    Departamento.find_by_nombre(nombre).delete
    dpto = Departamento.find_by_sql("SELECT COUNT(*) AS cantidad FROM departamentos WHERE nombre = '" + nombre + "'")
    assert_not_equal dpto[0].cantidad, nombre
  end

  test "edit tiempo departamento" do
    nombre = 'administracion'
    tiempo = 80
    ExtensionesHelper.crearDepartamento(nombre)
    dpto = Departamento.where("nombre = '" + nombre + "'")
    ExtensionesHelper.editTimeDpto(dpto[0].id, tiempo)
    dptoEdited = Departamento.where("nombre = '" + nombre + "'")
    time = dptoEdited[0].tiempo_timbrado
    assert_equal tiempo, time
  end

  test "crear orden de timbrado" do
    esperado = '5'
    result = ''
    crearDepartamento('tecnico')
    crearExtension('101')
    idDpto = Departamento.find_by_nombre('tecnico').id
    obj = Extension.find_by_sql("SELECT * FROM extensions WHERE appdata LIKE 'i=$%' AND exten LIKE 's"+idDpto.to_s+"'")
    result = obj[0].priority
    assert_equal(esperado, result)
  end

  test "crear orden de timbrado 2" do
    esperado = '$[${i} > 0]'
    result = ''
    nombre = 'comercial'
    crearDepartamento(nombre)
    crearOrdenDeTimbradoDeDptos
    idDpto = Departamento.find_by_nombre(nombre).id
    obj = Extension.find_by_sql("SELECT * FROM extensions WHERE app = 'While' AND exten LIKE 's"+idDpto.to_s+"'")
    result = obj[0].appdata
    assert_equal(esperado, result)
  end

  test "editar modo departamento" do
    nombre = 'tecnico'
    modo = 's'
    crearDepartamento(nombre)
    idDpto = Departamento.find_by_nombre(nombre).id
    editModeDpto(idDpto, modo)
    mode = Departamento.find_by_nombre(nombre).modo_timbrado
    assert_equal(modo, mode)
  end

end
